import { call, put } from "redux-saga/effects";
import { Types } from "../ducks/login";
import api from "../../services/api";
import AsyncStorage from "@react-native-community/async-storage";

export function* login(action) {
  const { login } = action;
  try {
    let response = yield call(api.post, "/users/auth/sign_in", {
      email: login.username,
      password: login.password
    });

    const { uid, client } = response.headers;
    const accessToken = response.headers["access-token"];
    yield AsyncStorage.multiSet([
      ["@CredentialsEmpresas:accessToken", accessToken],
      ["@CredentialsEmpresas:client", client],
      ["@CredentialsEmpresas:uid", uid]
    ]);

    yield put({
      type: Types.LOGIN_SUCCESS
    });
  } catch (response) {
    const { errors } = response.response.data;
    yield put({
      type: Types.LOGIN_ERROR,
      error: errors
    });
  }
}
