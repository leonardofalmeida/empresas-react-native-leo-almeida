import { all, fork, takeLatest } from "redux-saga/effects";
import { login } from "./login";
import { companyList, getCompany } from "./companyList";
import { Types as LoginTypes } from "../ducks/login";
import { Types as CompanyListTypes } from "../ducks/companyList";

export default function* rootSaga() {
  yield all([
    takeLatest(LoginTypes.LOGIN_REQUEST, login),
    takeLatest(CompanyListTypes.COMPANY_LIST_REQUEST, companyList),
    takeLatest(CompanyListTypes.GET_COMPANY_REQUEST, getCompany)
  ]);
}
