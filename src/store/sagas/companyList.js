import { call, put } from "redux-saga/effects";
import { Types } from "../ducks/companyList";
import api from "../../services/api";
import AsyncStorage from "@react-native-community/async-storage";

export function* companyList() {
  try {
    const accessToken = yield AsyncStorage.getItem(
      "@CredentialsEmpresas:accessToken"
    );
    const uid = yield AsyncStorage.getItem("@CredentialsEmpresas:uid");
    const client = yield AsyncStorage.getItem("@CredentialsEmpresas:client");

    let { data } = yield call(api.get, "/enterprises", {
      headers: {
        ["uid"]: uid,
        ["access-token"]: accessToken,
        ["client"]: client
      }
    });

    yield put({
      type: Types.COMPANY_LIST_SUCCESS,
      items: data.enterprises
    });
  } catch (response) {
    yield put({
      type: Types.COMPANY_LIST_ERROR,
      error: response
    });
  }
}

export function* getCompany(action) {
  try {
    const accessToken = yield AsyncStorage.getItem(
      "@CredentialsEmpresas:accessToken"
    );
    const uid = yield AsyncStorage.getItem("@CredentialsEmpresas:uid");
    const client = yield AsyncStorage.getItem("@CredentialsEmpresas:client");

    let { data } = yield call(api.get, `/enterprises/${action.id}`, {
      headers: {
        ["uid"]: uid,
        ["access-token"]: accessToken,
        ["client"]: client
      }
    });

    yield put({
      type: Types.GET_COMPANY_SUCCESS,
      item: data.enterprise
    });
  } catch (error) {}
}
