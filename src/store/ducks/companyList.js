const INITIAL_STATE = {
  error: false,
  errorMessage: null,
  loading: true,
  items: [],
  item: []
};

export const Types = {
  COMPANY_LIST_REQUEST: "companyList/COMPANY_LIST_REQUEST",
  COMPANY_LIST_SUCCESS: "companyList/COMPANY_LIST_SUCCESS",
  COMPANY_LIST_ERROR: "companyList/COMPANY_LIST_ERROR",
  GET_COMPANY_REQUEST: "companyList/GET_COMPANY_REQUEST",
  GET_COMPANY_SUCCESS: "companyList/GET_COMPANY_SUCCESS"
};

export default function companyList(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.COMPANY_LIST_SUCCESS:
      return {
        ...state,
        items: action.items
      };
    case Types.COMPANY_LIST_ERROR:
      return {
        ...state,
        error: true,
        errorMessage: action.error,
        loading: false
      };
    case Types.GET_COMPANY_SUCCESS:
      return {
        ...state,
        item: action.item
      };
    default:
      return state;
  }
}

export const Creators = {
  getCompanyList: () => ({
    type: Types.COMPANY_LIST_REQUEST
  }),
  getCompany: id => ({
    type: Types.GET_COMPANY_REQUEST,
    id
  })
};
