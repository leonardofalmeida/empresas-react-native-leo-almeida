import { combineReducers } from "redux";
import login from "./login";
import companies from "./companyList";

const reducers = combineReducers({
  login,
  companies
});

export default reducers;
