const INITIAL_STATE = {
  error: false,
  errorMessage: null,
  loading: false,
  response: {}
};

export const Types = {
  LOGIN_REQUEST: "login/LOGIN_REQUEST",
  LOGIN_SUCCESS: "login/LOGIN_SUCCESS",
  LOGIN_ERROR: "login/LOGIN_ERROR",
  CLEAR_LOGIN_ERROR: "login/CLEAR_LOGIN_ERROR"
};

export default function login(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.LOGIN_SUCCESS:
      return {
        ...state,
        error: false,
        errorMessage: null,
        loading: false
      };
    case Types.LOGIN_ERROR:
      return {
        ...state,
        error: true,
        errorMessage: action.error,
        loading: false
      };
    case Types.CLEAR_LOGIN_ERROR:
      return {
        ...state,
        error: false,
        errorMessage: null,
        loading: false
      };
    default:
      return state;
  }
}

export const Creators = {
  handleLogin: login => ({
    type: Types.LOGIN_REQUEST,
    login
  }),
  handleErrorLogin: () => ({
    type: Types.CLEAR_LOGIN_ERROR
  })
};
