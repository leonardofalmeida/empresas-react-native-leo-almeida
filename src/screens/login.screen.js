import React, { Component } from "react";

import { Creators as LoginActions } from "../store/ducks/login";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import Button from "../components/button.component";

import {
  KeyboardAvoidingView,
  TextInput,
  View,
  StyleSheet,
  Platform,
  Alert
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/FontAwesome";

const mapStateToProps = state => ({
  error: state.login.error,
  errorMessage: state.login.errorMessage,
  loading: state.login.loading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(LoginActions, dispatch);

class Login extends Component {
  static navigationOptions = {
    title: "Login"
  };

  state = {
    username: "",
    password: ""
  };

  handleInputUsername = username => {
    this.setState({
      username: username
    });
  };

  handleInputPassword = password => {
    this.setState({
      password: password
    });
  };

  handleSubmit = async () => {
    const { username, password } = this.state;
    if (!username.length) return;
    if (!password.length) return;

    this.props.handleLogin(this.state);

    const accessToken = await AsyncStorage.getItem(
      "@CredentialsEmpresas:accessToken"
    );
    const uid = await AsyncStorage.getItem("@CredentialsEmpresas:uid");
    const client = await AsyncStorage.getItem("@CredentialsEmpresas:client");

    if (accessToken !== null && uid !== null && client !== null) {
      this.props.navigation.navigate("Home");
    }
  };

  async componentDidMount() {
    const accessToken = await AsyncStorage.getItem(
      "@CredentialsEmpresas:accessToken"
    );
    const uid = await AsyncStorage.getItem("@CredentialsEmpresas:uid");
    const client = await AsyncStorage.getItem("@CredentialsEmpresas:client");

    if (accessToken !== null && uid !== null && client !== null) {
      this.props.navigation.navigate("Home");
    }
  }
  render() {
    const { errorMessage, handleErrorLogin } = this.props;
    return (
      <KeyboardAvoidingView
        behavior={Platform.select({
          ios: () => "padding",
          android: () => null
        })()}
        style={styles.container}
      >
        {!!errorMessage &&
          Alert.alert("Erro", JSON.stringify(errorMessage[0]), [
            { text: "OK", onPress: () => handleErrorLogin() }
          ])}
        <View style={styles.content}>
          <View>
            <Icon name="building-o" size={64} color="#000" />
          </View>
          <TextInput
            keyboardType="email-address"
            style={styles.inputUsername}
            onChangeText={this.handleInputUsername}
            placeholder="Email"
            returnKeyType="next"
          />
          <TextInput
            name="password"
            secureTextEntry={true}
            style={styles.inputPassword}
            onChangeText={this.handleInputPassword}
            placeholder="Senha"
            returnKeyType="send"
          />
          <Button text={"Entrar"} onPress={this.handleSubmit} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 30
  },
  inputUsername: {
    borderWidth: 1,
    borderColor: "#DDD",
    borderRadius: 4,
    height: 44,
    paddingHorizontal: 15,
    alignSelf: "stretch",
    marginTop: 30
  },
  inputPassword: {
    borderWidth: 1,
    borderColor: "#DDD",
    borderRadius: 4,
    height: 44,
    paddingHorizontal: 15,
    alignSelf: "stretch",
    marginTop: 5
  }
});
