import React, { Component } from "react";

import { Creators as CompanyListActions } from "../store/ducks/companyList";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { View, Text, StyleSheet } from "react-native";

const mapStateToProps = state => ({
  company: state.companies.item
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(CompanyListActions, dispatch);

class CompanyDetails extends Component {
  static navigationOptions = {
    title: "Detalhes"
  };

  render() {
    const { company } = this.props;
    return (
      <View style={styles.content}>
        <Text style={styles.text}>Descrição: {company.description}</Text>
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyDetails);

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 30
  },
  text: {
    color: "#000",
    fontSize: 16
  }
});
