import React, { Component } from "react";

import { Creators as CompanyListActions } from "../store/ducks/companyList";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import Button from "../components/button.component";
import Card from "../components/card.component";

import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableNativeFeedback
} from "react-native";

import AsyncStorage from "@react-native-community/async-storage";

const mapStateToProps = state => ({
  companies: state.companies.items,
  error: state.companies.error,
  errorMessage: state.companies.errorMessage,
  loading: state.companies.loading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(CompanyListActions, dispatch);

class CompanyList extends Component {
  static navigationOptions = {
    title: "Empresas"
  };

  async componentDidMount() {
    await this.props.getCompanyList();
  }

  handleClickEnterprise = async id => {
    await this.props.getCompany(id);
    this.props.navigation.navigate("Details");
  };

  handleSignOut = async () => {
    await AsyncStorage.removeItem("@CredentialsEmpresas:accessToken");
    await AsyncStorage.removeItem("@CredentialsEmpresas:client");
    await AsyncStorage.removeItem("@CredentialsEmpresas:uid");

    const accessToken = await AsyncStorage.getItem(
      "@CredentialsEmpresas:accessToken"
    );
    const client = await AsyncStorage.getItem("@CredentialsEmpresas:client");
    const uid = await AsyncStorage.getItem("@CredentialsEmpresas:uid");

    if (accessToken === null && client === null && uid === null)
      this.props.navigation.navigate("Auth");
  };

  render() {
    const { companies, errorMessage } = this.props;
    return (
      <View>
        {!!errorMessage && alert(errorMessage)}
        <Button text={"Sair"} onPress={this.handleSignOut} />
        <FlatList
          data={companies}
          numColumns={1}
          keyExtractor={item => item.id}
          renderItem={({ item }) => {
            return (
              <TouchableNativeFeedback
                onPress={() => {
                  this.handleClickEnterprise(item.id);
                }}
              >
                <View>
                  <Card>
                    <Text style={styles.text}>
                      Empresa: {item.enterprise_name}
                    </Text>
                    <Text style={styles.text}>Cidade: {item.city}</Text>
                    <Text style={styles.text}>País: {item.country}</Text>
                  </Card>
                </View>
              </TouchableNativeFeedback>
            );
          }}
        />
      </View>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyList);

const styles = StyleSheet.create({
  text: {
    color: "#000",
    fontSize: 16
  }
});
