import {
  createAppContainer,
  createSwitchNavigator,
  createStackNavigator
} from "react-navigation";
import Login from "./screens/login.screen";
import CompanyList from "./screens/companyList.screen";
import CompanyDetails from "./screens/companyDetails.screen";

const AppStack = createStackNavigator({
  Home: CompanyList,
  Details: CompanyDetails
});

const AuthStack = createStackNavigator({ Auth: Login });

const Routes = createAppContainer(
  createSwitchNavigator({
    Auth: AuthStack,
    App: AppStack
  })
);

export default Routes;
