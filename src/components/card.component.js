import React from "react";

import { View, StyleSheet } from "react-native";

const Card = props => {
  return (
    <View style={styles.cards}>
      <View style={styles.content}>{props.children}</View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  cards: {
    flex: 1,
    backgroundColor: "#FFF",
    borderWidth: 1,
    margin: 10,
    borderRadius: 4
  },
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 30
  }
});
